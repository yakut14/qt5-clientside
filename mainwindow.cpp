#include "mainwindow.h"

#include <QPushButton>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPlainTextEdit>
#include <QRegExpValidator>
#include <QFileDialog>
#include <QMessageBox>
#include <QTcpSocket>

#define TEXT_LABEL "Количество вхождений: "

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{
    QWidget* central = new QWidget( this );
    setCentralWidget( central );
    QVBoxLayout* vl = new QVBoxLayout;
    QHBoxLayout* hl = new QHBoxLayout;

    central->setLayout( vl );

    QPushButton* btGetData = new QPushButton( "Загрузить данные", central );
    btGetData->setObjectName( "btGetData" );
    QPushButton* btSendData = new QPushButton( "Найти вхождения", central );
    btSendData->setObjectName( "btSendData" );

    QLabel* lbAns = new QLabel( QString::fromStdString( TEXT_LABEL ) + "0", central );
    lbAns->setObjectName( "lbAns" );

    QRegExp experssion( "[a-zA-Zа-яА-Яёъй0-9]*" );
    QRegExpValidator* validator = new QRegExpValidator( experssion, central );
    QLineEdit* leWord = new QLineEdit( central );
    leWord->setObjectName( "leWord" );
    leWord->setValidator( validator );

    QPlainTextEdit* te = new QPlainTextEdit( central );
    te->setObjectName( "te" );
    te->setReadOnly( true );

    vl->addLayout( hl );
    vl->addWidget( te );
    hl->addWidget( btGetData );
    hl->addWidget( leWord );
    hl->addWidget( lbAns );
    hl->addWidget( btSendData );

    connect( btGetData, SIGNAL( pressed() ), this, SLOT( slotOnGetData() ) );
    connect( btSendData, SIGNAL( pressed() ), this, SLOT( slotOnSendData() ) );


    m_tcpSocket = new QTcpSocket( this );
    m_in.setDevice( m_tcpSocket );
    m_in.setVersion( QDataStream::Qt_4_0 );

    connect( m_tcpSocket, SIGNAL( readyRead() ), this,SLOT(readServerData()) );
    connect( m_tcpSocket, QOverload<QAbstractSocket::SocketError>::of(&QAbstractSocket::error), this, &MainWindow::displayError);
}

MainWindow::~MainWindow()
{

}


void MainWindow::slotOnGetData()
{
    QString fName = QFileDialog::getOpenFileName( this, "text file", QString(),"*.txt" );
    if( fName.isEmpty() )
        return;

    QPlainTextEdit* te = centralWidget()->findChild< QPlainTextEdit* >( "te" );
    if( !te )
        return;

    QFile f( fName );
    if( f.open( QIODevice::ReadOnly ) )
    {
        te->setPlainText( f.readAll() );
        f.close();
    }
    else
    {
        te->setPlainText( "" );
    }

    m_filePath = fName;
}

void MainWindow::slotOnSendData()
{
    QLineEdit* leWord = centralWidget()->findChild< QLineEdit* >( "leWord" );
    if( !leWord )
        return;

    QString curWord = leWord->text();
    QFile f( m_filePath );
    if( m_filePath.isEmpty() )
    {
        QMessageBox::information(this, tr("Client"), tr("Задайте файл"));
        m_prevWord = "";
        return;
    }
    else if( curWord.isEmpty() )
    {
        QMessageBox::information(this, tr("Client"), tr("Задайте слово для поиска"));
        m_prevWord = "";
        return;
    }
    else if( m_prevWord == curWord )
    {
        QMessageBox::information(this, tr("Client"), tr("Заданное слово уже искалось"));
        return;
    }
    else if( !f.open( QIODevice::ReadOnly ) )
    {
        QMessageBox::information(this, tr("Client"), tr("Файл не может быть открыт"));
        m_prevWord = "";
        return;
    }

    QPushButton* bt = centralWidget()->findChild< QPushButton* >( "btSendData" );
    if( !bt )
        return;

    m_tcpSocket->abort();
    m_tcpSocket->connectToHost( "127.0.0.1", 44470 );
    if( m_tcpSocket->waitForConnected() )
    {
        bt->setEnabled( false );
         m_prevWord = curWord;
        //sendOneMessage( f.readAll(), curWord );

        sendFileMessage( &f );
    }

    //f.close();
}

void MainWindow::sendFileMessage( QFile* openedFile )
{
    m_file = openedFile;
    //connect(m_tcpSocket, SIGNAL(bytesWritten(qint64)), this, SLOT(sendPartOfFile()));

    QByteArray block;//( QString::number( openedFile->size() ).toStdString().c_str() );
    QDataStream out( &block, QIODevice::WriteOnly );
    out.setVersion( QDataStream::Qt_4_0 );
    out.device()->seek(0);
    //out << quint16(0) << QString::number( openedFile->size() );
    //out << QString::number( openedFile->size() ) << quint16(0) ;
    int fileSize = openedFile->size();
    out << QString::number( fileSize ) << m_prevWord;


    QByteArray fData = m_file->readAll();
    block.append( fData );

    out.device()->seek( 0 );
    int x = 0;
    while( x < fileSize )
    {
        qint64 y = m_tcpSocket->write( block );
        x += y;
    }
/*
    m_tcpSocket->write( block );
    if( !m_tcpSocket->waitForBytesWritten() )
    {
        fprintf( stderr, "Error" );
        return;
    }

    //sendPartOfFile();
    */

    //Test
    QPushButton* bt = centralWidget()->findChild< QPushButton* >( "btSendData" );
    if( !bt )
        return;

    m_file->close();
    bt->setEnabled( true );
}

void MainWindow::sendPartOfFile()
{
    bool isEnd = m_file->atEnd();
    if( !isEnd  )
    {
       // char block[ 5 ];
       // int in = m_file->read( block, 5 );
        QByteArray block;//( m_file->read( 5 ) );
        QDataStream out( &block, QIODevice::WriteOnly );
        out.setVersion( QDataStream::Qt_4_0 );
        //out << quint16(0);
        out.device()->seek(0);
        out << quint16(0) << QString( m_file->read( 1024 ) )
           ;// << quint16(0);//quint16(block.size() - sizeof(quint16));
        m_tcpSocket->write( block );
        if( !m_tcpSocket->waitForBytesWritten() )
        {
            fprintf( stderr, "Error" );
            return;
        }
    }


    if(  m_file->atEnd() )
    {
        m_file->close();
        disconnect(m_tcpSocket, SIGNAL(bytesWritten(qint64)), this, SLOT(sendPartOfFile()));
        QByteArray block;//( m_prevWord.toStdString().c_str() );
        QDataStream out( &block, QIODevice::WriteOnly );
        out.setVersion( QDataStream::Qt_4_0 );
        out.device()->seek(0);
        out << quint16(0) << m_prevWord;
        m_tcpSocket->write( block );
        m_tcpSocket->waitForBytesWritten();

        QPushButton* bt = centralWidget()->findChild< QPushButton* >( "btSendData" );
        if( !bt )
            return;

        bt->setEnabled( true );
    }

}


void MainWindow::sendOneMessage(const QString &text, const QString & curWord )
{
    QString str = " #" + curWord;
    QByteArray w( str.toStdString().c_str() );
    QByteArray block( QString( text + str ).toStdString().c_str() );
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_0);

    m_tcpSocket->write( block );
    m_tcpSocket->flush();
}

void MainWindow::readServerData()
{
    QLabel* lb = centralWidget()->findChild< QLabel* >( "lbAns" );
    if( !lb )
        return;

    QPushButton* bt = centralWidget()->findChild< QPushButton* >( "btSendData" );
    if( !bt )
        return;

    QString inputdata;
    inputdata = m_tcpSocket->readAll();

    lb->setText( QString::fromStdString( TEXT_LABEL ) + inputdata );
    bt->setEnabled( true );
}

void MainWindow::displayError(QAbstractSocket::SocketError socketError)
{
    switch (socketError) {
    case QAbstractSocket::RemoteHostClosedError:
        break;
    case QAbstractSocket::HostNotFoundError:
        QMessageBox::information(this, tr("Client"),
                                 tr("Сервер по заданному адресу отсутствует"));
        break;
    case QAbstractSocket::ConnectionRefusedError:
        QMessageBox::information(this, tr("Client"),
                                 tr("Соединение с сервером разорвано"));
        break;
    default:
        QMessageBox::information(this, tr("Client"),
                                 tr("Ошибка: %1.")
                                 .arg( m_tcpSocket->errorString()));
    }

    QPushButton* bt = centralWidget()->findChild< QPushButton* >( "btSendData" );
    if( !bt )
        return;

    bt->setEnabled( true );
}
