#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QAbstractSocket>
#include <QDataStream>

class QTcpSocket;
class QTcpSocket;
class QNetworkSession;
class QFile;


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected slots:
    void slotOnGetData();
    void slotOnSendData();

    void displayError( QAbstractSocket::SocketError socketError );
    void readServerData();

    void sendPartOfFile();

protected:
    void sendOneMessage( const QString&, const QString& );
    void sendFileMessage( QFile* );

protected:
    QString m_filePath;
    QString m_prevWord;

    QDataStream m_in;
    QNetworkSession* m_networkSession;
    QTcpSocket* m_tcpSocket;

    QFile* m_file;
};

#endif // MAINWINDOW_H
